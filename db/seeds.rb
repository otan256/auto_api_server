def random_string
	o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
	string = (0...50).map { o[rand(o.length)] }.join
	string
end

App::Models::Session.create({
    :token => '3d2fc99e52427888a8c02c96bbe8e313',
    :login => 'test_user@mail.com',
    :last_accessed => Time.now,
    :created_at => Time.now,
    :session_duration => 256,
    :updated_at => Time.now,
  }
)

10.times do 
	App::Models::Car.create({
		:vin_code => rand.to_s[2..6].to_i,     
		:user_id => rand.to_s[2],
	  :name => random_string,
	  :model => random_string,
	  :manufacture_year => DateTime.new(1900 + rand.to_s[2..3].to_i),
	  :volume => rand.to_s[2..3].to_s,
	  :mileage => rand.to_s[2..7],
	  }
	)
end

10.times do 
	App::Models::Employee.create({
		:first_name => random_string,     
		:last_name => random_string,
	  :address => random_string,
	  :passport_number => random_string,
	  :position => random_string,
	  :birthday => DateTime.new(1900 + rand.to_s[2..3].to_i),
	  :phone => rand(2).to_s,
	  }
	)
end

10.times do 
	App::Models::Order.create({
		:employee_id => rand.to_s[2],     
		:product_id => rand.to_s[2],     
		:user_id => rand.to_s[2],     
		:service_id => rand.to_s[2],     
		:quantity => rand.to_s[2..4].to_i,
	  :price => rand.to_s[2..5],
	  :mark_up => random_string,
	  :order_date => DateTime.new(1900 + rand.to_s[2..3].to_i),
	  }
	)
end

10.times do 
	App::Models::Product.create({
		:quantity => rand.to_s[2],       
		:mark_up => random_string,
	  :price => random_string,
	  :name => random_string,
	  :arriving_date => DateTime.new(1900 + rand.to_s[2..3].to_i),
	  }
	)
end

10.times do 
	App::Models::Service.create({     
		:kind_of_service => random_string,
	  :standart_hours => random_string,
	  :standart_hours_price => random_string,
	  :labor_input_work => random_string,
	  :car_model => random_string,
	  }
	)
end
