# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141202114643) do

  create_table "cars", force: true do |t|
    t.integer  "vin_code"
    t.string   "name"
    t.string   "model"
    t.datetime "manufacture_year"
    t.string   "volume"
    t.integer  "mileage"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "user_id"
  end

  create_table "employees", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address"
    t.string   "passport_number"
    t.string   "position"
    t.datetime "birthday"
    t.string   "phone"
    t.string   "configs",         default: "default"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.string   "product_id"
    t.string   "user_id"
    t.string   "service_id"
    t.datetime "order_date"
    t.integer  "quantity"
    t.string   "price"
    t.string   "mark_up"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "employee_id"
  end

  create_table "products", force: true do |t|
    t.datetime "arriving_date"
    t.string   "name"
    t.integer  "quantity"
    t.string   "price"
    t.string   "mark_up"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "services", force: true do |t|
    t.string   "kind_of_service"
    t.string   "standart_hours"
    t.string   "standart_hours_price"
    t.string   "labor_input_work"
    t.string   "car_model"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", id: false, force: true do |t|
    t.string   "user_id"
    t.string   "login",            null: false
    t.string   "token",            null: false
    t.datetime "last_accessed"
    t.integer  "session_duration"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["created_at"], name: "index_sessions_on_created_at", using: :btree
  add_index "sessions", ["login"], name: "index_sessions_on_login", using: :btree

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "phone"
    t.string   "address"
    t.string   "password_hash"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "permissions",                 default: 0
    t.datetime "birthday"
    t.string   "configs",                     default: "default"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
    t.text     "oauth_token"
    t.time     "oauth_expires_at"
    t.string   "locale",           limit: 15
  end

end
