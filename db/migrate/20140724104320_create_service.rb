class CreateService < ActiveRecord::Migration
  def up
    create_table :services do |t|
      t.string :kind_of_service
      t.string :standart_hours
      t.string :standart_hours_price
      t.string :labor_input_work
      t.string :car_model

      t.timestamps
    end
  end

  def down
    drop_table :services
  end
end