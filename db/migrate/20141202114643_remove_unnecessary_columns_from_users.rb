class RemoveUnnecessaryColumnsFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :car_number
    remove_column :users, :car_model
    remove_column :users, :vin_code
  end

end
