class CreateShop < ActiveRecord::Migration
  def up
    create_table :shops do |t|
      t.datetime :arriving_date
      t.string :name
      t.integer :quantity
      t.string :price
      t.string :mark_up

      t.timestamps
    end
  end

  def down
    drop_table :shops
  end
end