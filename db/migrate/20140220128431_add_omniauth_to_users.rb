class AddOmniauthToUsers < ActiveRecord::Migration
  def up
    add_column :users, :provider, :string
    add_column :users, :uid, :string
    add_column :users, :oauth_token, :text
    add_column :users, :oauth_expires_at, :time
    add_column :users, :locale, :string, :limit => 15
  end

  def down
    drop_table :users
  end
end
