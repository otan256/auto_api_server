class CreateCars < ActiveRecord::Migration
  def up
    create_table :cars do |t|
      t.integer :vin_code
      t.string :name
      t.string :model
      t.datetime :manufacture_year
      t.string :volume
      t.integer :mileage

      t.timestamps
    end
  end

  def down
    drop_table :cars
  end
end