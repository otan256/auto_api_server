class CreateStorage < ActiveRecord::Migration
  def up
    create_table :storages do |t|
      t.string :product_id
      t.datetime :arriving_date
      t.string :name
      t.integer :quantity
      t.string :price

      t.timestamps
    end
  end

  def down
    drop_table :storages
  end
end