class AddSessions < ActiveRecord::Migration
  def up
    create_table :sessions, :primary_key => 'token', :force => true, :id => false do |t|
      t.string :user_id
      t.string :login,          :null => false
      t.string :token,          :null => false
      t.datetime :last_accessed
      t.integer :session_duration
      t.string :status

      t.timestamps
    end
    add_index "sessions", ["created_at"], :name => "index_sessions_on_created_at"
    add_index "sessions", ["login"], :name => "index_sessions_on_login"

  end

  def down
    drop_table :sessions
  end
end