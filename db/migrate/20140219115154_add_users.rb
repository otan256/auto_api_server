class AddUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :email
      t.string :phone
      t.string :address
      t.string :password_hash
      t.string :first_name
      t.string :last_name
      t.integer :permissions, default: 0
      t.string :car_model
      t.string :car_number
      t.datetime :birthday
      t.string :phone
      t.integer :vin_code
      t.text :configs
      
      t.timestamps
    end
  end
 
  def down
    drop_table :users
  end
end
