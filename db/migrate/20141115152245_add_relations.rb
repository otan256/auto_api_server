class AddRelations < ActiveRecord::Migration
  def up
    add_column :cars, :user_id, :string
    add_column :orders, :employee_id, :string
    remove_column :orders, :customer_id
    rename_table :shops, :products
    # remove_column :products, :product_id
    
    drop_table :storages
    create_table :employees do |t|
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :passport_number
      t.string :position
      t.datetime :birthday
      t.string :phone
      t.text :configs
      
      t.timestamps
    end
  end

end
