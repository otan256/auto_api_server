class CreateOrder < ActiveRecord::Migration
  def up
    create_table :orders do |t|
      t.string :customer_id
      t.string :product_id
      t.string :user_id
      t.string :service_id
      t.datetime :order_date
      t.integer :quantity
      t.string :price
      t.string :mark_up

      t.timestamps
    end
  end

  def down
    drop_table :orders
  end
end