module ApiServer
  class Employees < ApiServer::Base

    namespace '/v1/employees' do

      post '/create' do
        validate_params :call_code => App::Services::Employees.error_codes[:create_employee] do
          param :first_name, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :last_name, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :passport_number, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :position, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :birthday, :required => false, :type => :datetime
          param :phone, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :address, :required => false, :maxlength => 255, :type => :string
        end

        employees_service = App::Services::Employees.new
        entity = employees_service.create(params)

        present entity
      end

      post '/:id/update' do
        validate_params :call_code => App::Services::Cars.error_codes[:update_employee] do
          param :id, :required => true, :type => :string
          param :first_name, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :last_name, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :passport_number, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :position, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :birthday, :required => false, :type => :datetime
          param :phone, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :address, :required => false, :maxlength => 255, :type => :string
        end

        employees_service = App::Services::Employees.new
        entity = employees_service.update(params)

        present entity
      end

      get '/find' do
        validate_params :call_code => App::Services::Employees.error_codes[:find_employees] do
          param :first_name, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :last_name, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :passport_number, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :position, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :birthday, :required => false, :type => :datetime
          param :phone, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :address, :required => false, :maxlength => 255, :type => :string
        end

        employees_service = App::Services::Employees.new
        entity = employees_service.find(params)

        present entity
      end

      get '/:id' do
        validate_params :call_code => App::Services::Employees.error_codes[:get_employee] do
          param :id, :required => true, :type => :string
        end

        employees_service = App::Services::Employees.new
        entity = employees_service.get(params[:id])

        present entity
      end


    end
  end
end
