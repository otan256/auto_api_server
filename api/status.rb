module ApiServer
  class Status < ApiServer::Base

    namespace '/v1' do

      get '/status' do
        [200, {:status => 'ok'}.to_json]
      end

    end
  end
end
