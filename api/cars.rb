module ApiServer
  class Cars < ApiServer::Base

    namespace '/v1/cars' do

      post '/create' do
        validate_params :call_code => App::Services::Cars.error_codes[:create_car] do
          param :vin_code, :required => true, :type => :numeric
          param :user_id, :required => true, :type => :string
          param :name, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :model, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :manufacture_year, :required => false, :type => :date
          param :volume, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :mileage, :required => true, :type => :numeric
        end

        cars_service = App::Services::Cars.new
        entity = cars_service.create(params)

        present entity
      end

      post '/:id/update' do
        validate_params :call_code => App::Services::Cars.error_codes[:update_car] do
          param :id, :required => true, :type => :string
          param :vin_code, :required => false, :type => :numeric
          param :user_id, :required => false, :type => :string
          param :name, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :model, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :manufacture_year, :required => false, :type => :date
          param :volume, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :mileage, :required => false, :type => :numeric
        end

        cars_service = App::Services::Cars.new
        entity = cars_service.update(params)

        present entity
      end

      get '/find' do
        validate_params :call_code => App::Services::Cars.error_codes[:find_cars] do
          param :vin_code, :required => false, :type => :numeric
          param :name, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :user_id, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :model, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :manufacture_year, :required => false, :type => :date
          param :manufacture_year_from, :required => false, :type => :date
          param :manufacture_year_to, :required => false, :type => :date
          param :volume, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :volume_from, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :volume_to, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :mileage, :required => false, :type => :numeric
          param :mileage_from, :required => false, :type => :numeric
          param :mileage_to, :required => false, :type => :numeric
        end

        cars_service = App::Services::Cars.new
        entity = cars_service.find(params)

        present entity
      end

      get '/:id' do
        validate_params :call_code => App::Services::Cars.error_codes[:get_car] do
          param :id, :required => true, :type => :string
        end

        cars_service = App::Services::Cars.new
        entity = cars_service.get(params[:id])

        present entity
      end


    end
  end
end
