module ApiServer
  class Services < ApiServer::Base

    namespace '/v1/services' do

      post '/create' do
        validate_params :call_code => App::Services::Services.error_codes[:create_service] do
          param :kind_of_service, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :standart_hours, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :standart_hours_price, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :labor_input_work, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :car_model, :required => true, :minlength => 1, :maxlength => 255, :type => :string
        end

        services_service = App::Services::Services.new
        entity = services_service.create(params)

        present entity
      end

      get '/find' do
        validate_params :call_code => App::Services::Services.error_codes[:find_services] do
          param :kind_of_service, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :standart_hours, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :standart_hours_from, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :standart_hours_to, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :standart_hours_price, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :standart_hours_price_from, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :standart_hours_price_to, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :labor_input_work, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :labor_input_work_from, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :labor_input_work_to, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :car_model, :required => false, :minlength => 1, :maxlength => 255, :type => :string
        end

        services_service = App::Services::Services.new
        entity = services_service.find(params)

        present entity
      end

      get '/:id' do
        validate_params :call_code => App::Services::Services.error_codes[:get_service] do
          param :id, :required => true, :type => :string
        end

        services_service = App::Services::Services.new
        entity = services_service.get(params[:id])

        present entity
      end


    end
  end
end
