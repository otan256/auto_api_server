module ApiServer
  class Authentication < ApiServer::Base

    namespace '/v1/authenticate' do

      get '/login_facebook' do
        validate_params :call_code => App::Services::Authentication.error_codes[:login_user_facebook] do
          param :provider, :required => true, :minlength => 7, :maxlength => 9, :type => :string
          param :uid, :required => true, :minlength => 10, :maxlength => 255, :type => :string
          param :credentials, :required => true, :type => :hash
          param :info, :required => true, :type => :hash
        end

        authentication_service = App::Services::Authentication.new
        entity = authentication_service.authenticate_facebook_user(params)

        present entity
      end

      post '/login' do
        validate_params :call_code => App::Services::Authentication.error_codes[:login_user] do
          param :email, :required => true, :regex => /\A.+@.+\..+\z/, :minlength => 3, :maxlength => 50, :type => :string
          param :password, :required => true, :minlength => 1, :maxlength => 255, :type => :string
        end

        authentication_service = App::Services::Authentication.new
        entity = authentication_service.authenticate_user(params[:email], params[:password])

        present entity
      end

      post '/register' do
        validate_params :call_code => App::Services::Authentication.error_codes[:register_user] do
          param :email, :required => true, :regex => /\A.+@.+\..+\z/, :minlength => 1, :maxlength => 50, :type => :string
          param :password, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :first_name, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :last_name, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :birthday, :required => false, :minlength => 1, :maxlength => 255, :type => :date
          param :phone, :required => false, :minlength => 7, :maxlength => 12, :type => :string
          param :address, :required => false, :minlength => 5, :maxlength => 255, :type => :string
        end

        authentication_service = App::Services::Authentication.new
        entity = authentication_service.create_user(params)

        present entity
      end

    end

  end
end