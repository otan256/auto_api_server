module ApiServer
  class Products < ApiServer::Base

    namespace '/v1/products' do

      post '/create' do
        validate_params :call_code => App::Services::Products.error_codes[:create_product] do
          param :name, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :arriving_date, :required => true, :type => :datetime
          param :price, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :mark_up, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :quantity, :required => true, :type => :numeric
        end

        products_service = App::Services::Products.new
        entity = products_service.create(params)

        present entity
      end

      get '/find' do
        validate_params :call_code => App::Services::Products.error_codes[:find_products] do
          param :name, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :arriving_date, :required => false, :type => :datetime
          param :arriving_date_from, :required => false, :type => :datetime
          param :arriving_date_to, :required => false, :type => :datetime
          param :price, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :price_from, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :price_to, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :mark_up, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :quantity, :required => false, :type => :numeric
          param :quantity_from, :required => false, :type => :numeric
          param :quantity_to, :required => false, :type => :numeric
        end

        products_service = App::Services::Products.new
        entity = products_service.find(params)

        present entity
      end

      get '/:id' do
        validate_params :call_code => App::Services::Products.error_codes[:get_product] do
          param :id, :required => true, :type => :string
        end

        products_service = App::Services::Products.new
        entity = products_service.get(params[:id])

        present entity
      end


    end
  end
end
