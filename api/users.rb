module ApiServer
  class Users < ApiServer::Base

    namespace '/v1/users' do

       post '/:id/update' do
        validate_params :call_code => App::Services::Cars.error_codes[:update_user] do
          param :id, :required => true, :type => :string
          param :first_name, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :last_name, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :birthday, :required => false, :type => :datetime
          param :phone, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :address, :required => false, :maxlength => 255, :type => :string
        end

        cars_service = App::Services::Cars.new
        entity = cars_service.update(params)

        present entity
      end

      get '/find' do
        validate_params :call_code => App::Services::Users.error_codes[:find_users] do
          param :first_name, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :last_name, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :email, :required => false, :regex => /\A.+@.+\..+\z/, :type => :string
          param :birthday, :required => false, :type => :datetime
          param :birthday_from, :required => false, :type => :datetime
          param :birthday_to, :required => false, :type => :datetime
          param :phone, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :address, :required => false, :type => :string
        end

        users_service = App::Services::Users.new
        entity = users_service.find(params)

        present entity
      end

      get '/send_mail/:id' do
        validate_params :call_code => App::Services::Users.error_codes[:get_user] do
          param :id, :required => true, :type => :string
          param :subject, :required => true, :type => :string
        end

        subject = params[:subject]

        Pony.mail(
            :to => 'otan256@gmail.com',
            :subject => subject,
            :body => 'sdfsdfsdfsdfsdfsd',
        )
      end

      get '/:id' do
        validate_params :call_code => App::Services::Users.error_codes[:get_user] do
          param :id, :required => true, :type => :string
        end

        users_service = App::Services::Users.new
        entity = users_service.get(params[:id])

        present entity
      end

    end
  end
end
