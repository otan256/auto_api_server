module ApiServer
  class Orders < ApiServer::Base

    namespace '/v1/orders' do

      post '/create' do
        validate_params :call_code => App::Services::Orders.error_codes[:create_order] do
          param :employee_id, :required => false, :type => :string
          param :product_id, :required => false, :type => :string
          param :user_id, :required => true, :type => :string
          param :service_id, :required => false, :type => :string
          param :price, :required => true, :minlength => 1, :maxlength => 255, :type => :string
          param :mark_up, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :order_date, :required => false, :type => :datetime
          param :quantity, :required => true, :type => :numeric
        end

        orders_service = App::Services::Orders.new
        entity = orders_service.create(params)

        present entity
      end

      get '/find' do
        validate_params :call_code => App::Services::Orders.error_codes[:find_orders] do
          param :user_id, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :price, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :price_from, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :price_to, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :mark_up, :required => false, :minlength => 1, :maxlength => 255, :type => :string
          param :order_date, :required => false, :type => :datetime
          param :order_date_from, :required => false, :type => :datetime
          param :order_date_to, :required => false, :type => :datetime
          param :quantity, :required => false, :type => :numeric
          param :quantity_from, :required => false, :type => :numeric
          param :quantity_to, :required => false, :type => :numeric
        end

        orders_service = App::Services::Orders.new
        entity = orders_service.find(params)

        present entity
      end

      get '/:id' do
        validate_params :call_code => App::Services::Orders.error_codes[:get_order] do
          param :id, :required => true, :type => :string
        end

        orders_service = App::Services::Orders.new
        entity = orders_service.get(params[:id])

        present entity
      end


    end
  end
end
