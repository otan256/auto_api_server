require 'i18n'
require 'i18n/backend/fallbacks'

module ApiServer
  class Base < Sinatra::Base
    set :show_exceptions => false
    set :protection, false

    UNTOKENIZED_CALLS = %w|authenticate status|

    before do
      content_type 'application/json'
      #check_token unless UNTOKENIZED_CALLS.include? current_namespace
      I18n.locale = params[:locale] || I18n.default_locale
      headers 'Access-Control-Allow-Origin' => '*',
              'Access-Control-Allow-Methods' => ['OPTIONS', 'GET', 'POST'],
              'Access-Control-Allow-Headers' => '*'
    end

    after do
      ActiveRecord::Base.clear_active_connections!
    end

    options '/*' do
      response["Access-Control-Allow-Headers"] = "origin, x-requested-with, content-type, x-auth-token"
    end

    helpers Sinatra::JSON
    helpers Sinatra::ContentFor
    helpers App::Tools::ValidateParams
    helpers App::Tools::PresentEntities
    register Sinatra::Namespace

    configure :development do
      register Sinatra::Reloader

      Dir[File.expand_path('./*.rb', __FILE__)].each { |f| also_reload f }

      %w(models responses services tools).each do |dir|
        Dir[File.expand_path("../../app/#{dir}/*.rb", __FILE__)].each { |f| also_reload f }
      end
    end

    configure do
      I18n::Backend::Simple.send(:include, I18n::Backend::Fallbacks)
      I18n.load_path = Dir[File.join(settings.root, 'locales', '*.yml')]
      I18n.backend.load_translations
      # Comma separate list of remote hosts that are allowed.
      # :any will allow any host
      set :allow_origin, :any

      # HTTP methods allowed
      set :allow_methods, [:get, :post]

      # Allow cookies to be sent with the requests
      set :allow_credentials, true

      enable :cross_origin
    end

    error ParameterValidationError do
      validator = env['sinatra.error'].validator

      [validator.http_response_code, validator.as_json]
    end

    not_found do
      [404, {
        :error_code => :resource_not_found,
        :error_messages => {
            :base => [
                {
                  :code => :resource_not_found,
                  :message => 'Resource not found'
                }
            ]
        }
      }.to_json]
    end

    def session
      @session ||= if env['HTTP_X_AUTH_TOKEN']
                     App::Models::Session.load_session(env['HTTP_X_AUTH_TOKEN'])
                   end
    end

    private

    def check_token
      unless session
        halt 403, {'Content-Type' => 'application/json'}, App::Errors::AuthenticationError.cry.to_json
      end
    end

    # Parses uri and gets first value after 'v2' e.g.
    # /v2/contact/details/master/12345 => 'contact'
    # /v2/status => 'status'
    def current_namespace
      uri = request.env['REQUEST_URI']
      uri ||= request.env['PATH_INFO']
      uri.split('/')[4]
    end
  end
end
