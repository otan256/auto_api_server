require File.expand_path('../config/environment', __FILE__)

map "/" do
  use ApiServer::Authentication
  use ApiServer::Cars
  use ApiServer::Users
  use ApiServer::Orders
  use ApiServer::Services
  use ApiServer::Employees
  use ApiServer::Products
  run ApiServer::Base
end