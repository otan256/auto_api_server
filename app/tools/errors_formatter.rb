module App
  module Tools
    module ErrorsFormatter

      def get_generic_error(error_code, params)
        messages = params.keys.each_with_object({}) do |key, hash|
          hash[key] = [
              App::Models::FieldError.new(
                  :code => error_code,
                  :message => I18n.t("errors.contacts.#{error_code}"),
                  :params => {key => params[key]}
              )
          ]
        end

        App::Models::ErrorResponse.new({
                                           :error_code => error_code,
                                           :error_messages => messages
                                       })
      end

      def get_auth_error
        App::Models::AuthenticationErrorResponse.produce
      end

      def get_unavailable_error
        App::Models::UnavailableErrorResponse.produce
      end

      def get_empty_request_error
        App::Models::EmptyRequestErrorResponse.produce
      end

    end
  end
end
