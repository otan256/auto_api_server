module App
  module Tools
    module ValidateParams

      class ParameterValidationError < StandardError
        attr_reader :validator

        def initialize(validator)
          @validator = validator
        end
      end

      class BaseParameterValidator

        attr_reader :attr, :value, :options, :scope, :param
        attr_accessor :error_code, :error_message, :error_params

        def initialize(attr, value, options, scope)
          @attr           = attr
          @value          = value
          @options        = options
          @param          = options.is_a?(Hash) ? options[:param] : options
          @scope          = scope

          @error_code     = ''
          @error_message  = ''
          @error_params   = {}

          setup(attr, value, options)
        end

        def valid?
          if options.is_a?(Hash) && ( options[:if] || options[:unless] )
            condition = options[:if] || options[:unless]

            should_validate = if condition.is_a? Symbol
                                scope.public_send condition
                              else
                                condition.call(attr, value)
                              end

            should_validate = !should_validate if options[:unless]
          else
            should_validate = true
          end

          if should_validate
            validate
          else
            true
          end
        end

        def validate
          raise NotImplementedError
        end

        def setup(attr, value, options)
          raise NotImplementedError
        end

        private

        def i18n_key
          'params_validation_errors'
        end
      end

      class RangeParameterValidator < BaseParameterValidator
        def setup(attr, value, options)
          key = 'in_range'
          self.error_code     = "#{attr}_#{key}"
          self.error_params   = {:range => param.to_s}
          self.error_message  = I18n.t("#{i18n_key}.#{key}", self.error_params.merge(:attr => attr))
        end

        def validate
          options.include? value
        end
      end

      class LengthParameterValidator < BaseParameterValidator
        def setup(attr, value, options)
          key = 'length_is_invalid'
          self.error_code     = "#{attr}_#{key}"
          self.error_params   = {:length => param}
          self.error_message  = I18n.t("#{i18n_key}.#{key}", self.error_params.merge(:attr => attr))
        end

        def validate
          value.to_s.length == options
        end
      end

      class RegexParameterValidator < BaseParameterValidator
        def setup(attr, value, options)
          key = 'does_not_match_regex'
          self.error_code     = "#{attr}_#{key}"
          self.error_message  = I18n.t("#{i18n_key}.#{key}", {:attr => attr})
        end

        def validate
          !!(value =~ options) # force the boolean value
        end
      end

      class TypeParameterValidator < BaseParameterValidator
        def setup(attr, value, options)
          key = 'type_is_wrong'
          self.error_code     = "#{attr}_#{key}"
          self.error_params   = { :type => param.to_s }
          self.error_message  = I18n.t("#{i18n_key}.#{key}", self.error_params.merge(:attr => attr))
        end

        def validate
          case options
            when :string
              is_string?
            when :integer
              is_integer?
            when :boolean
              is_boolean?
            when :date
              is_date?
            else
              true
          end
        end

        def is_date?
          # regex for date format ISO 8601
          # force boolean
          !!(value =~ /\A([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?\Z/)
        end

        def is_string?
          true
        end

        def is_integer?
          value.to_i.to_s == value
        end

        def is_boolean?
          !!(value =~ /\Atrue|false\Z/i)  # force boolean
        end
      end

      class RequiredParameterValidator < BaseParameterValidator
        def setup(attr, value, options)
          key = 'is_required'
          self.error_code     = "#{attr}_#{key}"
          self.error_message  = I18n.t("#{i18n_key}.#{key}", {:attr => attr})
        end

        def validate
          if param == false
            true # if :require => false, validation returns true
          else
            !(value.nil?)
          end
        end
      end

      class MaxlengthParameterValidator < BaseParameterValidator
        def setup(attr, value, options)
          key = 'is_too_long'
          self.error_code     = "#{attr}_#{key}"
          self.error_params   = {:maxlength => param}
          self.error_message  = I18n.t("#{i18n_key}.#{key}", self.error_params.merge(:attr => attr))
        end

        def validate
          value.to_s.length <= options
        end
      end

      class MinlengthParameterValidator < BaseParameterValidator
        def setup(attr, value, options)
          key = 'is_too_short'
          self.error_code     = "#{attr}_#{key}"
          self.error_params   = {:minlength => param}
          self.error_message  = I18n.t("#{i18n_key}.#{key}", self.error_params.merge(:attr => attr))
        end

        def validate
          value.to_s.length >= options
        end
      end

      class AtLeastOneParameterValidator < BaseParameterValidator
        def setup(attr, value, options)
          key = 'at_least_one_parameter'
          self.error_code     = key
          self.error_params   = {:parameters => self.param.join(', ')}
          self.error_message  = I18n.t("#{i18n_key}.#{key}", self.error_params)
        end

        def validate
          self.param.any? { |attr| value.has_key? attr }
        end
      end

      class ParamsValidationService

        attr_accessor :errors, :http_response_code, :call_code

        def initialize(options = {})
          @errors = {}
          @call_code = options.fetch(:call_code, 'server_error')
          @http_response_code = options.fetch(:http_response_code, 400)
        end

        def as_json
          {'error_code' => call_code, 'error_messages' => errors}.to_json
        end

        def valid?
          self.errors.empty?
        end

        def add_error(attr, error_hash = {})
          self.errors ||= {}
          self.errors[attr] ||= []
          self.errors[attr] << {
              :code     => error_hash.fetch(:error_code, ''),
              :message  => error_hash.fetch(:error_message, ''),
              :params   => error_hash.fetch(:error_params, ''),
          }
        end
      end

      def validate_params(options={}, &block)
        @validation_service = options.fetch(:validation_service, ParamsValidationService).new(options)

        yield if block_given?

        unless @validation_service.valid?
          raise ParameterValidationError.new(@validation_service)
        end
      end

      def param(attr, options={})
        optional = options.delete(:optional)

        # Handle default value
        if options.has_key?(:default)
          default_value = options.delete(:default)
          if params[attr].nil?
            params[attr] = default_value
          end
        end

        if options.has_key?(:optional)
          options[:required] = !options.delete(:optional)
        end

        if (options[:required] == false || optional) && (params[attr].nil? || params[attr].to_s == '')
          # we skip empty parameter if it is not required or optional
        else
          options.each do |validator_name, validation_param|
            validator = find_validator(validator_name).new(attr, params[attr], validation_param, self)
            unless validator.valid?
              @validation_service.add_error(attr, {
                  :error_code     => validator.error_code,
                  :error_message  => validator.error_message,
                  :error_params   => validator.error_params,
              })
            end
          end
        end
      end

      def validate(options={})
        options.each do |validator_name, validation_param|
          validator = find_validator(validator_name).new(:base, params, validation_param, self)
          unless validator.valid?
            @validation_service.add_error(:base, {
                :error_code     => validator.error_code,
                :error_message  => validator.error_message,
                :error_params   => validator.error_params,
            })
          end
        end
      end

      def find_validator(validator_name)
        "App::Tools::ValidateParams::#{validator_name.to_s.classify}ParameterValidator".constantize
      end

    end
  end
end
