module App
  module Tools
    module PresentEntities

      def present(object)
        [object.http_status_code, object.to_json]
      end

    end
  end
end
