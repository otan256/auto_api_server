module App
  module Responses
    class AuthenticateResponse < ApiResponse

      attribute :id, String
      attribute :first_name, String
      attribute :last_name, String
      attribute :email, String
      attribute :token, String
      attribute :created_at, Date
      attribute :updated_at, Date

    end
  end
end