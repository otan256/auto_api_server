module App
  module Responses
    class OrdersResponse < ApiResponse
      attribute :orders, Array[App::Responses::OrderResponse]
    end
  end
end