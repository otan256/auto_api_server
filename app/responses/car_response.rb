module App
  module Responses
    class CarResponse < ApiResponse

      attribute :id, String
      attribute :vin_code, Integer
      attribute :name, String
      attribute :user_id, String
      attribute :model, String
      attribute :manufacture_year, Date
      attribute :volume, String
      attribute :mileage, Integer
      attribute :created_at, Date
      attribute :updated_at, Date

    end
  end
end