module App
  module Responses
    class EmployeeResponse < ApiResponse

      attribute :id, String
      attribute :first_name, String
      attribute :last_name, String
      attribute :passport_number, String
      attribute :position, String
      attribute :address, String
      attribute :birthday, Date
      attribute :phone, String
      attribute :created_at, Date
      attribute :updated_at, Date

      # Relation attributes
      attribute :orders, Array[App::Responses::OrderResponse]

    end
  end
end