module App
  module Responses
    class CarsResponse < ApiResponse
      attribute :cars, Array[App::Responses::CarResponse]
    end
  end
end