module App
  module Responses
    class UsersResponse < ApiResponse
      attribute :users, Array[App::Responses::UserResponse]
    end
  end
end