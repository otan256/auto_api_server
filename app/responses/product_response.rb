require File.expand_path('../../responses/order_response.rb', __FILE__)

module App
  module Responses
    class ProductResponse < ApiResponse

      attribute :id, String
      attribute :name, String
      attribute :arriving_date, Date
      attribute :price, String
      attribute :mark_up, String
      attribute :quantity, Integer
      attribute :created_at, Date
      attribute :updated_at, Date
      # Relation attributes
      attribute :orders, Array[App::Responses::OrderResponse]
    end
  end
end