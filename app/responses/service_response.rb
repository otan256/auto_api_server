require File.expand_path('../../responses/order_response.rb', __FILE__)

module App
  module Responses
    class ServiceResponse < ApiResponse

      attribute :id, String
      attribute :kind_of_service, String
      attribute :standart_hours, String
      attribute :standart_hours_price, String
      attribute :labor_input_work, String
      attribute :car_model, String
      attribute :created_at, Date
      attribute :updated_at, Date
      # Relation attributes
      attribute :orders, Array[App::Responses::OrderResponse]
    end
  end
end