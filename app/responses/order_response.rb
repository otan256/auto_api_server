module App
  module Responses
    class OrderResponse < ApiResponse

      attribute :id, String
      attribute :employee_id, String
      attribute :product_id, String
      attribute :user_id, String
      attribute :service_id, String
      attribute :quantity, Integer
      attribute :price, String
      attribute :order_date, Date
      attribute :mark_up, String
      attribute :created_at, Date
      attribute :updated_at, Date
      # Relation attributes
      attribute :user, Hash
      attribute :product, Hash
      attribute :service, Hash
      attribute :employee, Hash
    end
  end
end