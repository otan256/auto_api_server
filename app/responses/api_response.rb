module App
  module Responses
    class ApiResponse
      include Virtus.model

      def http_status_code
        200
      end

      def mapping
        {}
      end

    end
  end
end