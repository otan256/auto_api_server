module App
  module Responses
    class ProductsResponse < ApiResponse
      attribute :products, Array[App::Responses::ProductResponse]
    end
  end
end