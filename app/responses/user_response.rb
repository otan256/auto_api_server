require File.expand_path('../../responses/car_response.rb', __FILE__)
require File.expand_path('../../responses/order_response.rb', __FILE__)

module App
  module Responses
    class UserResponse < ApiResponse

      attribute :id, String
      attribute :first_name, String
      attribute :last_name, String
      attribute :car_model, String
      attribute :car_number, String
      attribute :vin_code, String
      attribute :birthday, Date
      attribute :phone, String
      attribute :address, String
      attribute :created_at, Date
      attribute :updated_at, Date
      # Relation attributes
      attribute :cars, Array[App::Responses::CarResponse]
      attribute :orders, Array[App::Responses::OrderResponse]

    end
  end
end
