module App
  module Responses
    class EmployeesResponse < ApiResponse
      attribute :employees, Array[App::Responses::EmployeeResponse]
    end
  end
end