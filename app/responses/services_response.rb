module App
  module Responses
    class ServicesResponse < ApiResponse
      attribute :services, Array[App::Responses::ServiceResponse]
    end
  end
end