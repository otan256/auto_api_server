module App
  module Models
    class Order < ActiveRecord::Base

      belongs_to :user
      belongs_to :service
      belongs_to :product
      belongs_to :employee

      def create_from_params(params)
        transaction do
          self.employee_id = params[:employee_id]
          self.product_id = params[:product_id]
          self.user_id = params[:user_id]
          self.service_id = params[:service_id]
          self.price = params[:price]
          self.mark_up = params[:mark_up]
          self.quantity = params[:quantity]
          self.order_date = DateTime.new(params[:order_date].to_i)

          self.save
        end
      end

      def with_relations
        self.as_json.merge({
          :user => user.as_json,
          :service => service.as_json,
          :product => product.as_json,
          :employee => user.as_json,
        })
      end

      def self.get_by_id(id)
        where(:id => id).first
      end

      def self.search(params)
        search_results(params).map(&:with_relations).map(&:as_json)
      end

      private

      def self.search_results(params)
        scope = self.all

        scope = scope.where('price = ?', params[:price])                 unless params[:price].blank?
        scope = scope.where('user_id = ?', params[:user_id])             unless params[:user_id].blank?
        scope = scope.where('price >= ?', params[:price_from])           unless params[:price_from].blank?
        scope = scope.where('price <= ?', params[:price_to])             unless params[:price_to].blank?
        scope = scope.where('mark_up = ?', params[:mark_up])             unless params[:mark_up].blank?
        scope = scope.where('order_date = ?', params[:order_date])       unless params[:order_date].blank?
        scope = scope.where('order_date >= ?', params[:order_date_from]) unless params[:order_date_from].blank?
        scope = scope.where('order_date <= ?', params[:order_date_to])   unless params[:order_date_to].blank?
        scope = scope.where('quantity = ?', params[:quantity])           unless params[:quantity].blank?
        scope = scope.where('quantity >= ?', params[:quantity_from])     unless params[:quantity_from].blank?
        scope = scope.where('quantity <= ?', params[:quantity_to])       unless params[:quantity_to].blank?

        scope
      end

    end
  end
end