module App
  module Models
    class Car < ActiveRecord::Base

      belongs_to :user

      validates_presence_of :vin_code, :name, :manufacture_year, :volume, :user_id
      validates_uniqueness_of :vin_code

      def create_from_params(params)
        transaction do
          self.vin_code = params[:vin_code]
          self.user_id = params[:user_id]
          self.name = params[:name]
          self.model = params[:model]
          self.manufacture_year = DateTime.new(params[:manufacture_year].to_i)
          self.volume = params[:volume]
          self.mileage = params[:mileage]

          self.save
        end
      end

      def update_from_params(params)
        transaction do
          self.vin_code = params[:vin_code] if params[:vin_code]
          self.name = params[:name] if params[:name]
          self.model = params[:model] if params[:model]
          self.manufacture_year = DateTime.new(params[:manufacture_year].to_i) if params[:manufacture_year]
          self.volume = params[:volume] if params[:volume]
          self.mileage = params[:mileage] if params[:mileage]

          self.save
        end
      end

      def self.get_by_id(id)
        where(:id => id).first
      end

      def self.search(params)
        search_results(params).map(&:as_json)
      end

      private

      def self.search_results(params)
        scope = self.all

        scope = scope.where('vin_code = ?', params[:vin_code])                           unless params[:vin_code].blank?
        scope = scope.where('name = ?', params[:name])                                   unless params[:name].blank?
        scope = scope.where('user_id = ?', params[:user_id])                             unless params[:user_id].blank?
        scope = scope.where('model = ?', params[:model])                                 unless params[:model].blank?
        scope = scope.where('manufacture_year = ?', params[:manufacture_year])           unless params[:manufacture_year].blank?
        scope = scope.where('manufacture_year >= ?', params[:manufacture_year_from])     unless params[:manufacture_year_from].blank?
        scope = scope.where('manufacture_year <= ?', params[:manufacture_year_to])       unless params[:manufacture_year_to].blank?
        scope = scope.where('volume >= ?', params[:volume_from])                         unless params[:volume_from].blank?
        scope = scope.where('volume <= ?', params[:volume_to])                           unless params[:volume_to].blank?
        scope = scope.where('volume = ?', params[:volume])                               unless params[:volume].blank?
        scope = scope.where('mileage >= ?', params[:mileage_from])                       unless params[:mileage_from].blank?
        scope = scope.where('mileage <= ?', params[:mileage_to])                         unless params[:mileage_to].blank?
        scope = scope.where('mileage = ?', params[:mileage])                             unless params[:mileage].blank?

        scope
      end



    end
  end
end
