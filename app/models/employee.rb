module App
  module Models
    class Employee < ActiveRecord::Base

      has_many :orders

      validates_uniqueness_of :passport_number, :phone

      def create_from_params(params)
        transaction do
          self.first_name = params[:first_name]
          self.last_name = params[:last_name]
          self.passport_number = params[:passport_number]
          self.position = params[:position]
          self.phone = params[:phone]
          self.address = params[:address]
          self.birthday = DateTime.new(params[:birthday].to_i)

          self.save
        end
      end

      def update_from_params(params)
        transaction do
          self.first_name = params[:first_name] if params[:first_name]
          self.last_name = params[:last_name] if params[:last_name]
          self.passport_number = params[:passport_number] if params[:passport_number]
          self.position = params[:position] if params[:position]
          self.phone = params[:phone] if params[:phone]
          self.address = params[:address] if params[:address]
          self.birthday = DateTime.new(params[:birthday].to_i) if params[:birthday]

          self.save
        end
      end

      def with_relations
        self.as_json.merge({:orders => orders.map(&:as_json)})
      end

      def self.get_by_id(id)
        where(:id => id).first
      end

      def self.search(params)
        search_results(params).map(&:as_json)
      end

      private

      def self.search_results(params)
        scope = self.all

        scope = scope.where('first_name = ?', params[:first_name])              unless params[:first_name].blank?
        scope = scope.where('last_name = ?', params[:last_name])                unless params[:last_name].blank?
        scope = scope.where('passport_number = ?', params[:passport_number])    unless params[:passport_number].blank?
        scope = scope.where('position = ?', params[:position])                  unless params[:position].blank?
        scope = scope.where('address LIKE ?', "%#{params[:address]}")           unless params[:address].blank?
        scope = scope.where('birthday = ?', params[:birthday])                  unless params[:birthday].blank?
        scope = scope.where('phone = ?', params[:phone])                        unless params[:phone].blank?

        scope
      end

    end
  end
end