module App
  module Models
    class Product < ActiveRecord::Base

      has_many :orders

      def create_from_params(params)
        transaction do
          self.name = params[:name]
          self.price = params[:price]
          self.mark_up = params[:mark_up]
          self.quantity = params[:quantity]
          self.arriving_date = DateTime.new(params[:arriving_date].to_i)

          self.save
        end
      end

      def with_relations
        self.as_json.merge({:orders => orders.map(&:as_json)})
      end

      def self.get_by_id(id)
        where(:id => id).first
      end

      def self.search(params)
        search_results(params).map(&:as_json)
      end

      private

      def self.search_results(params)
        scope = self.all

        scope = scope.where('name = ?', params[:name])                          unless params[:name].blank?
        scope = scope.where('arriving_date = ?', params[:arriving_date])        unless params[:arriving_date].blank?
        scope = scope.where('arriving_date >= ?', params[:arriving_date_from])  unless params[:arriving_date_from].blank?
        scope = scope.where('arriving_date <= ?', params[:arriving_date_to])    unless params[:arriving_date_to].blank?
        scope = scope.where('price = ?', params[:price])                        unless params[:price].blank?
        scope = scope.where('price >= ?', params[:price_from])                  unless params[:price_from].blank?
        scope = scope.where('price <= ?', params[:price_to])                    unless params[:price_to].blank?
        scope = scope.where('quantity = ?', params[:quantity])                  unless params[:quantity].blank?
        scope = scope.where('quantity >= ?', params[:quantity_from])            unless params[:quantity_from].blank?
        scope = scope.where('quantity <= ?', params[:quantity_to])              unless params[:quantity_to].blank?
        scope = scope.where('mark_up = ?', params[:mark_up])                    unless params[:mark_up].blank?

        scope
      end

    end
  end
end