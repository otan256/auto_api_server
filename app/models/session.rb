module App
  module Models
    class Session < ActiveRecord::Base

      self.primary_key = :token

      def remove_expired_sessions
        self.class.where('login = ? and last_accessed < ?', login, (last_accessed - session_duration.seconds)).delete_all
      end

      def expired?
        exp = Time.now.utc > (last_accessed + session_duration.seconds)
        delete if exp

        exp
      end

      def self.create_new_session(user)
        @token = generate_token

        details = formatted_response(user).merge(:last_accessed => Time.now.utc, :token => @token)
        App::Models::Session.create(details)
      end

      def self.formatted_response(user)
        {
            :login => user.email,
            :session_duration => (20 * 60) #default to 20 mins if not set
        }
      end

      def self.load_session(token)
        current_session = self.where(:token => token).first
        return nil unless current_session && !current_session.expired?

        current_session.touch(:last_accessed)
        RequestStore.store[:session] = current_session
        store_principal_id(current_session)
        current_session
      end

      def self.store_principal_id(current_session)
        if current_session.user_id
          RequestStore.store[:principal_identifier] = {
              :principal_identifier => {
                  :user_id           => current_session.user_id,
                  :effective_user_id => current_session.user_id,
              }
          }
        end
      end

      def self.generate_token
        SecureRandom.hex(16)
      end

    end
  end
end