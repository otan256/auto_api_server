require 'active_record'
# md5 is needed for password generation
require 'bcrypt'

module App
  module Models
    class User < ActiveRecord::Base
      include BCrypt

      has_many :cars
      has_many :orders

      validates_presence_of :email, :password_hash, :first_name, :last_name
      validates_uniqueness_of :email

      scope :birthdays, -> { where(:birthday => Time.now.strftime("%d/%m/%Y")) }
      scope :get_by_id, -> (id) { where(:id => id) }

      def password
        @password ||= Password.new(password_hash)
      end

      def password=(new_password)
        @password = Password.create(new_password)
        self.password_hash = @password
      end

      def generate_token!
        SecureRandom.hex(16)
      end

      def with_relations
        self.as_json.merge({:cars => cars.map(&:as_json), :orders => orders.map(&:as_json)})
      end

      def self.authenticate(email, password)
        user = self.where(email: email).first
        if user && user.password == password
          user
        else
          nil
        end
      end

      def self.from_facebook(auth)
        where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
          user.provider = auth['provider']
          user.uid = auth['uid']
          user.email = auth['info']['email']
          user.first_name = auth['info']['first_name']
          user.last_name = auth['info']['last_name']
          user.address = auth['info']['location']
          user.locale = auth['extra']['raw_info']['locale']
          user.oauth_token = auth['credentials']['token']
          user.oauth_expires_at = Time.at(auth['credentials']['expires_at'].to_i)
          user.save!
        end
      end

      def update_from_params(params)
        transaction do
          self.first_name = params[:first_name] if params[:first_name]
          self.last_name = params[:last_name] if params[:last_name]
          self.car_model = params[:car_model] if params[:car_model]
          self.car_number = params[:car_number] if params[:car_number]
          self.vin_code = params[:vin_code] if params[:vin_code]
          self.birthday = params[:birthday] if params[:birthday]
          self.phone = params[:phone] if params[:phone]
          self.address = params[:address] if params[:address]

          self.save
        end
      end

      def self.search(params)
        search_results(params).map(&:as_json)
      end

      #Used for cron job
      def self.send_birthday_congrats
        birthdays.each do |user|
          ##
        end
      end

      private

      def self.search_results(params)
        scope = self.all

        scope = scope.where('first_name = ?', params[:first_name])     unless params[:first_name].blank?
        scope = scope.where('last_name = ?', params[:last_name])       unless params[:last_name].blank?
        scope = scope.where('email = ?', params[:email])               unless params[:email].blank?
        scope = scope.where('car_model = ?', params[:car_model])       unless params[:car_model].blank?
        scope = scope.where('car_number = ?', params[:car_number])     unless params[:car_number].blank?
        scope = scope.where('vin_code = ?', params[:vin_code])         unless params[:vin_code].blank?
        scope = scope.where('birthday = ?', params[:birthday])         unless params[:birthday].blank?
        scope = scope.where('birthday >= ?', params[:birthday_from])   unless params[:birthday_from].blank?
        scope = scope.where('birthday >= ?', params[:birthday_to])     unless params[:birthday_to].blank?
        scope = scope.where('phone = ?', params[:phone])               unless params[:phone].blank?
        scope = scope.where('address = ?', params[:address])           unless params[:address].blank?

        scope
      end


    end
  end
end