module App
  module Models
    class Service < ActiveRecord::Base

      has_many :orders

      def create_from_params(params)
        transaction do
          self.kind_of_service = params[:kind_of_service]
          self.standart_hours = params[:standart_hours]
          self.standart_hours_price = params[:standart_hours_price]
          self.labor_input_work = params[:labor_input_work]
          self.car_model = params[:car_model]

          self.save
        end
      end

      def with_relations
        self.as_json.merge({:orders => orders.map(&:as_json)})
      end

      def self.get_by_id(id)
        where(:id => id).first
      end

      def self.search(params)
        search_results(params).map(&:as_json)
      end

      private

      def self.search_results(params)
        scope = self.all

        scope = scope.where('kind_of_service = ?', params[:kind_of_service])                  unless params[:kind_of_service].blank?
        scope = scope.where('standart_hours = ?', params[:standart_hours])                    unless params[:standart_hours].blank?
        scope = scope.where('standart_hours >= ?', params[:standart_hours_from])              unless params[:standart_hours_from].blank?
        scope = scope.where('standart_hours <= ?', params[:standart_hours_to])                unless params[:standart_hours_to].blank?
        scope = scope.where('standart_hours_price = ?', params[:standart_hours_price])        unless params[:standart_hours_price].blank?
        scope = scope.where('standart_hours_price >= ?', params[:standart_hours_price_from])  unless params[:standart_hours_price_from].blank?
        scope = scope.where('standart_hours_price <= ?', params[:standart_hours_price_to])    unless params[:standart_hours_price_to].blank?
        scope = scope.where('labor_input_work = ?', params[:labor_input_work])                unless params[:labor_input_work].blank?
        scope = scope.where('labor_input_work >= ?', params[:labor_input_work_from])          unless params[:labor_input_work_from].blank?
        scope = scope.where('labor_input_work <= ?', params[:labor_input_work_to])            unless params[:labor_input_work_to].blank?
        scope = scope.where('car_model = ?', params[:car_model])                              unless params[:car_model].blank?

        scope
      end

    end
  end
end