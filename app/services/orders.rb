module App::Services
  class Orders
    include ::App::Tools::ErrorsFormatter

    def self.error_codes
      {
          :create_order => :create_order_failed,
          :get_order    => :get_order_failed,
          :find_orders  => :find_orders_failed,
      }
    end

    def get(id)
      order = App::Models::Order.get_by_id(id)
      if order
        full_order = order.with_relations
        App::Responses::OrderResponse.new(full_order)
      else
        App::Errors::OrderError.cry(:not_found)
      end
    end

    def create(params)
      order = App::Models::Order.new
      order.create_from_params(params)
      if order.valid?
        full_order = order.with_relations
        App::Responses::OrderResponse.new(full_order)
      else
        App::Errors::OrderError.cry_validation_error(:create_failed, order.errors.messages)
      end
    end

    def find(params)
      orders = App::Models::Order.search(params)
      if orders
        App::Responses::OrdersResponse.new({:orders => orders})
      else
        App::Errors::OrderError.cry(:not_found)
      end
    end

  end
end