module App::Services
  class Authentication
    include ::App::Tools::ErrorsFormatter

    def self.error_codes
      {
          :login_user => :auth_invalid_user_login_details,
          :login_user_facebook => :auth_invalid_user_facebook_details,
          :register_user => :auth_invalid_user_register_details
      }
    end

    def authenticate_user(email, password)
      user = App::Models::User.authenticate(email, password)
      if user
        token = create_session(user)
        response = user.as_json.merge!({:token => token})
        App::Responses::AuthenticateResponse.new(response)
      else
        App::Errors::AuthenticationError.cry
      end
    end

    def authenticate_facebook_user(params)
      user = App::Models::User.from_facebook(params)
      if user
        token = create_session(user)
        response = user.as_json.merge!({:token => token})
        App::Responses::AuthenticateResponse.new(response)
      else
        App::Errors::AuthenticationError.cry
      end
    end

    def create_user(params)
      user = App::Models::User.new(params)
      user.password = params[:password]
      if user.save
        App::Responses::AuthenticateResponse.new(user.as_json)
      else
        App::Errors::AuthenticationError.cry
      end
    end

    private
    def create_session(user)
      session = App::Models::Session.create_new_session(user)
      session.remove_expired_sessions
      session.token
    end

  end
end