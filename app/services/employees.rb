module App::Services
  class Employees
    include ::App::Tools::ErrorsFormatter

    def self.error_codes
      {
          :create_employee => :create_employee_failed,
          :update_employee => :update_employee_failed,
          :get_employee    => :get_employee_failed,
          :find_employees  => :find_employees_failed,
      }
    end

    def get(id)
      employee = App::Models::Employee.get_by_id(id)
      if employee
        full_employee = employee.with_relations
        App::Responses::EmployeeResponse.new(full_employee)
      else
        App::Errors::EmployeeError.cry(:not_found)
      end
    end

    def create(params)
      employee = App::Models::Employee.new
      employee.create_from_params(params)
      if employee.valid?
        full_employee = employee.with_relations
        App::Responses::EmployeeResponse.new(full_employee)
      else
        App::Errors::EmployeeError.cry_validation_error(:create_failed, employee.errors.messages)
      end
    end

    def update(params)
      employee = App::Models::Employee.get_by_id(params[:id])
      App::Errors::EmployeeError.cry(:not_found) unless employee 
      employee.update_from_params(params) 
      if employee.valid?
        App::Responses::EmployeeResponse.new(employee.as_json)
      else
        App::Errors::EmployeeError.cry_validation_error(:update_failed, employee.errors.messages)
      end
    end

    def find(params)
      employees = App::Models::Employee.search(params)
      if employees
        App::Responses::EmployeesResponse.new({:employees => employees})
      else
        App::Errors::EmployeeError.cry(:not_found)
      end
    end

  end
end