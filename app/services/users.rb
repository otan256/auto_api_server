module App::Services
  class Users
    include ::App::Tools::ErrorsFormatter

    def self.error_codes
      {
          :get_user    => :get_user_failed,
          :update_user => :update_user_failed,
          :find_users  => :find_users_failed,
      }
    end

    def get(id)
      user = App::Models::User.get_by_id(id).first
      if user
        full_user = user.with_relations
        App::Responses::UserResponse.new(full_user)
      else
        App::Errors::UserError.cry(:not_found)
      end
    end

    def update(params)
      user = App::Models::User.get_by_id(id).first
      App::Errors::UserError.cry(:not_found) unless user 
      user.update_from_params(params) 
      if user.valid?
        full_user = user.with_relations
        App::Responses::UserResponse.new(full_user)
      else
        App::Errors::UserError.cry(:not_found)
      end
    end

    def find(params)
      users = App::Models::User.search(params)
      if users
        App::Responses::UsersResponse.new({:users => users})
      else
        App::Errors::UserError.cry(:not_found)
      end
    end

  end
end