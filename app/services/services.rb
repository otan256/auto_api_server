module App::Services
  class Services
    include ::App::Tools::ErrorsFormatter

    def self.error_codes
      {
          :create_service => :create_service_failed,
          :get_service    => :get_service_failed,
          :find_services  => :find_services_failed,
      }
    end

    def get(id)
      service = App::Models::Service.get_by_id(id)
      if service
        full_service = service.with_relations
        App::Responses::ServiceResponse.new(full_service)
      else
        App::Errors::ServiceError.cry(:not_found)
      end
    end

    def create(params)
      service = App::Models::Service.new
      service.create_from_params(params)
      if service.valid?
        full_service = service.with_relations
        App::Responses::ServiceResponse.new(full_service)
      else
        App::Errors::ServiceError.cry_validation_error(:create_failed, service.errors.messages)
      end
    end

    def find(params)
      services = App::Models::Service.search(params)
      if services
        App::Responses::ServicesResponse.new({:services => services})
      else
        App::Errors::ServiceError.cry(:not_found)
      end
    end

  end
end