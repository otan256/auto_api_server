module App::Services
  class Products
    include ::App::Tools::ErrorsFormatter

    def self.error_codes
      {
          :create_product => :create_product_failed,
          :get_product    => :get_product_failed,
          :find_products  => :find_products_failed,
      }
    end

    def get(id)
      product = App::Models::Product.get_by_id(id)
      if product
        full_product = product.with_relations
        App::Responses::ProductResponse.new(full_product)
      else
        App::Errors::ProductError.cry(:not_found)
      end
    end

    def create(params)
      product = App::Models::Product.new
      product.create_from_params(params)
      if product.valid?
        full_product = product.with_relations
        App::Responses::ProductResponse.new(full_product)
      else
        App::Errors::ProductError.cry_validation_error(:create_failed, product.errors.messages)
      end
    end

    def find(params)
      products = App::Models::Product.search(params)
      if products
        App::Responses::ProductsResponse.new({:products => products})
      else
        App::Errors::ProductError.cry(:not_found)
      end
    end

  end
end