module App::Services
  class Cars
    include ::App::Tools::ErrorsFormatter

    def self.error_codes
      {
          :create_car => :create_car_failed,
          :update_car => :update_car_failed,
          :get_car    => :get_car_failed,
          :find_cars  => :find_cars_failed,
      }
    end

    def get(id)
      car = App::Models::Car.get_by_id(id)
      if car
        App::Responses::CarResponse.new(car.as_json)
      else
        App::Errors::CarError.cry(:not_found)
      end
    end

    def create(params)
      car = App::Models::Car.new
      car.create_from_params(params)
      if car.valid?
        App::Responses::CarResponse.new(car.as_json)
      else
        App::Errors::CarError.cry_validation_error(:create_failed, car.errors.messages)
      end
    end

    def update(params)
      car = App::Models::Car.get_by_id(params[:id])
      App::Errors::CarError.cry(:not_found) unless car 
      car.update_from_params(params) 
      if car.valid?
        App::Responses::CarResponse.new(car.as_json)
      else
        App::Errors::CarError.cry_validation_error(:update_failed, car.errors.messages)
      end
    end

    def find(params)
      cars = App::Models::Car.search(params)
      if cars
        App::Responses::CarsResponse.new({:cars => cars})
      else
        App::Errors::CarError.cry(:not_found)
      end
    end

  end
end