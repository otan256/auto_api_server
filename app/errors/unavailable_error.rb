module App
  module Errors
    class UnavailableErrorResponse < ErrorResponse
      def self.cry
        self.new({
           :error_code => :service_unavailable,
           :error_messages => {
               :base => [
                   App::Errors::FieldError.new(:code => :service_unavailable, :message => I18n.t("errors.unavailable.service_unavailable"))
               ]
           }
        })
      end

      def http_status_code
        503
      end
    end
  end
end
