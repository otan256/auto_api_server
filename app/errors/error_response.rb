module App
  module Errors
    class ErrorResponse
      include Virtus.model

      attribute :error_code, String
      attribute :error_messages, Hash


      def self.cry_validation_error(error_code, ar_error_model)
        self.new({
           :error_code => error_code,
           :error_messages => {
               :base => [
                   App::Errors::FieldError.new(:code => ar_error_model.keys, :message => ar_error_model.values.first, :params => ar_error_model.keys)
               ]
           }
       })
      end

      def http_status_code
        403
      end

      def entity_fields
        [:error_code, :error_messages]
      end

      def present
        entity = {}
        entity_fields = self.entity_fields

        entity_fields.each do |key|
          if self.respond_to?(key)
            entity[key] = self[key]
          else
            entity[key] = self[self.mapping[key]]
          end
        end
        entity
      end

    end
  end
end
