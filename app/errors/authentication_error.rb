module App
  module Errors
    class AuthenticationError < ErrorResponse

      def self.cry
        self.new({
           :error_code => :auth_failed,
           :error_messages => {
               :username => [
                   App::Errors::FieldError.new(:code => :invalid_supplied_credentials, :message => I18n.t("errors.authentication.invalid_supplied_credentials"))
               ]
           }
        })
      end

      def status
        500
      end

    end
  end
end