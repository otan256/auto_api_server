module App
  module Errors
    class FieldError
      include Virtus.model

      attribute :code, String
      attribute :message, String
      attribute :params, Hash
    end
  end
end
