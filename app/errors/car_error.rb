module App
  module Errors
    class CarError < ErrorResponse

      def self.cry(error_code)
        self.new({
           :error_code => error_code,
           :error_messages => {
               :base => [
                   App::Errors::FieldError.new(:code => :invalid_supplied_details, :message => I18n.t("errors.car.#{error_code}"))
               ]
           }
        })
      end

      def status
        500
      end

    end
  end
end