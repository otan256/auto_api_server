require 'rubygems'
require 'bundler/setup'
require "sinatra/reloader" if ENV['RACK_ENV'] == 'development'

APPNAME = 'Auto API Server'
ENV['RACK_ENV'] ||= "development"

Bundler.require :default, ENV['RACK_ENV']

[
    File.expand_path('../../app/responses/api_response.rb', __FILE__),
    File.expand_path('../../app/responses/authenticate_response.rb', __FILE__),
    File.expand_path('../../app/responses/order_response.rb', __FILE__),
    File.expand_path('../../app/responses/car_response.rb', __FILE__),
    File.expand_path('../../app/responses/product_response.rb', __FILE__),
    File.expand_path('../../app/responses/employee_response.rb', __FILE__),
    File.expand_path('../../app/responses/service_response.rb', __FILE__),
    File.expand_path('../../app/responses/user_response.rb', __FILE__),
    File.expand_path('../../app/errors/error_response.rb', __FILE__),
    File.expand_path('../../app/models/user.rb', __FILE__),
    File.expand_path('../../app/tools/errors_formatter.rb', __FILE__),
    File.expand_path('../../app/tools/present_entity.rb', __FILE__),
    File.expand_path('../../app/tools/validate_params.rb', __FILE__),
].each { |f| require f }

Dir[File.expand_path('../../app/*/*.rb', __FILE__)].each { |f| require f }

Dir[File.expand_path('../../api/base.rb', __FILE__)].each { |f| require f }
Dir[File.expand_path('../../api/**.rb', __FILE__)].each { |f| require f }

Dir[File.expand_path('../../app/**.rb', __FILE__)].each { |f| require f }

I18n.load_path += Dir[File.expand_path('../../config/locales/*.yml', __FILE__)]
I18n.backend.load_translations

ApiLogger = Class.new(Logger) { alias write << }
LOGGER = ApiLogger.new(STDOUT)

config = YAML.load(File.read(File.join(File.dirname(__FILE__), "#{ENV['RACK_ENV']}/database.yml")))

ActiveRecord::Base.logger = LOGGER
ActiveRecord::Base.establish_connection(config)

LOGGER.info "#{APPNAME} ready, env: #{ENV['RACK_ENV']}"
