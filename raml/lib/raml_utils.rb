import org.raml.parser.visitor.RamlDocumentBuilder
import org.raml.parser.visitor.YamlDocumentBuilder
import org.raml.parser.visitor.RamlValidationService
import org.raml.parser.loader.FileResourceLoader
import org.raml.parser.tagresolver.TagResolver

class RamlUtils
  attr_reader :filename, :options

  def initialize(options)
    @filename = options.raml_file
    @options = options
  end

  def validate
    @validation_results ||= validate_raml
  end

  def parse
    @parsed ||= Document.new(builder.build(file_contents), options)
  end

  def dump
    parse
    YamlDocumentBuilder.dumpFromAst(builder.getRootNode)
  end

  private
  def get_resource_loader(filename)
    FileResourceLoader.new(File.dirname(File.absolute_path(filename)))
  end

  def file_contents
    @contents ||= File.read(filename)
  end

  def builder
    @builder ||= RamlDocumentBuilder.new(get_resource_loader(filename))
  end

  def validate_raml
    loader = get_resource_loader(filename)
    validator = RamlValidationService.createDefault(loader, [].to_java(TagResolver))
    results = validator.validate(file_contents)

    if !results.empty?
      results = results.sort_by { |v| v.line }
      return results.map { |v| {message: "Line: #{v.line} - #{v.message}"} }
    end

    mime_types = parse.flat_resources.map { |resc| resc.actions.map { |a| a.responses.map { |resp| resp.mime_types }} }.flatten
    mime_types.select { |m| m.schema? && m.example? }.map do |m|
      message = begin
        JSON::Validator.validate!(m.schema.json, m.example, strict: true, validate_schema: true, insert_defaults: true)
        nil
      rescue JSON::ParserError => e
        e.message
      rescue JSON::Schema::ValidationError => e
        e.message
      end
      {verb: m.action.verb, status_code: m.response.status_code, url: m.action.resource.url, message: message } if message
    end.compact
  end
end


