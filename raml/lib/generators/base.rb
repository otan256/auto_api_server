class FormatterBase
  attr_reader :document, :resources, :output_dir
  def initialize(document)
    @document = document

    FileUtils.mkdir_p(document.options.output_dir) if document.options.generator != :console
    @output_dir = document.options.output_dir

    @resources = document.flat_resources
  end
end