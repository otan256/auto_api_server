class ConsoleGenerator < FormatterBase
  def generate
    template = File.read(File.join(File.dirname(__FILE__), 'template.mustache'))
    Mustache.render(template, :title => document.title, :resources => resources)
  end
end

