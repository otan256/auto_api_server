class ErrorHtmlGenerator < FormatterBase
  def generate
    template = File.read(File.join(File.dirname(__FILE__), 'template.mustache'))
    render_to_file(template,  "errors.html", :title => document.title, :resources => build_model)
  end

  private
  def render_to_file(template, filename, parameters)
    result = Mustache.render(template, parameters)
    File.open(File.join(output_dir, filename), 'w') { |file| file.write(result) }
  end

  def build_model
    resources.map do |resource|
      resource.actions.map do |action|
        {
          verb: action.verb,
          url: "/v2#{action.resource.url}",
          errors: action.error_responses.map do |response|
                    next unless response.default && response.default.example?
                    example = response.default.example
                    {
                      error_code: example["error_code"],
                      sub_errors: example["error_messages"].map do |property, errors|
                        errors.map do |error|
                          {
                            property: property,
                            code: error["code"],
                            message: error["message"],
                            params: error["params"] && !error["params"].empty? ? JSON.dump(error["params"]) : ''
                          }
                        end
                      end.flatten
                    }
                  end.compact
        }
      end
    end.flatten
  end
end