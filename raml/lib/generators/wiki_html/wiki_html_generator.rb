class WikiHtmlGenerator < FormatterBase
  URL_REGEX = /\/(\w+)?\/{0,1}/
  def generate
    template = read_template('resource')

    grouped_resources = resources.group_by { |r| URL_REGEX.match(r.url)[1] }
    links = []
    grouped_resources.each do |name, grouped_resource|
      filename = "#{name}.html"
      actions = get_view_model(grouped_resource)

      render_to_file(template, filename, :title => document.title, :group_name => name[0].upcase + name[1..-1], :actions => actions)
      links << {'name' => name, 'filename' => filename}
    end

    render_to_file(read_template('index'), 'index.html', :title => document.title, :links => links)
  end

  private
  def read_template(name)
    File.read(File.join(File.dirname(__FILE__), "#{name}.mustache"))
  end

  def render_to_file(template, filename, parameters)
    result = Mustache.render(template, parameters)
    File.open(File.join(output_dir, filename), 'w') { |file| file.write(result) }
  end

  def get_view_model(resources)
    actions = resources.map { |r| r.actions }.flatten
    actions.map do |action|
      view_model = {
        verb: action.verb,
        url: action.resource.url,
        display_name: action.resource.display_name,
        description: action.description,
        required_parameters: action.required_parameters,
        optional_parameters: action.optional_parameters,
        named_anchor: build_named_anchor(action)
      }
      view_model[:has_required_parameters] = !view_model[:required_parameters].empty?
      view_model[:has_optional_parameters] = !view_model[:optional_parameters].empty?

      ok_response = action.responses.find { |r| r.status_code == '200' }
      if ok_response
        view_model[:has_ok_response] = true
        default = ok_response.default
        schema = default.schema
        view_model[:response_properties] = build_properties(schema.json)

        if view_model[:has_example] = default.example?
          view_model[:example] = default.raml.example
        end
      end
      view_model
    end
  end

  def build_properties(object, property_list=nil, level=0)
    property_list ||= []
    properties = object['properties']
    return unless properties
    
    properties.each do |key, value|
      prefix = "&nbsp;" * (level * 2)
      delimiter = level == 0 ? '' : '- ' 
      property_list << {name: "#{prefix}#{delimiter}#{key}", description: value[:description]}
      case value['type']
      when 'array'
        raise "The type for the items have not been specified: '#{value['description']}'" unless value['items']
        build_properties(value['items'], property_list, level + 1)
      when 'object'
        build_properties(value, property_list, level + 1)
      end
    end

    property_list
  end

  def build_named_anchor(action)
    value = "#{action.verb} #{action.resource.url}"
    value.downcase.gsub(/[^\w\d]/, '_').gsub(/_+/, '_').chomp('_')
  end
end