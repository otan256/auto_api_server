class Options
  attr_reader :parser, :filter, :generator, :raml_file, :output_dir, :display_help, :dump
  def initialize
    @parser = OptionParser.new do |opts|
      opts.banner = "Usage: parse_raml.rb [options] --raml <RAML FILE>"

      opts.on("-f=", "--filter", "Filter by resources e.g. 'authenticate,balances'") do |v|
        @filter = v.split(',')
      end
      opts.on("-g=", "--generator", "Which generator to use (console, wikihtml)") { |v| @generator = v.to_sym }
      opts.on("-r=", "--raml RAML_FILE", "Location of base raml file") { |v| @raml_file = v }
      opts.on("-o=", "--output-dir", "The output directory for the generator") { |v| @output_dir = v }
      opts.on("-d", "--dump", "Dump raml specification as single file") { @dump = true}
      opts.on("-h", "--help", "Displays the help") { @display_help = true }
    end

    begin
      parser.parse!
      @display_help = true unless raml_file || display_help
    rescue OptionParser::MissingArgument => e
      puts e.message
      puts ""
      @display_help = true
    end

    #defaults
    @generator ||= :console
    @filter ||= []

    if generator == :console
      @output_dir = nil
    else
      @output_dir = File.expand_path(@output_dir)
    end
  end

  def banner
    parser.banner
  end
end