class FormParameter < RamlModel
  attr_reader :name, :description
  def initialize(name, form_parameter)
    super(form_parameter)
    @name = name
    @description = raml.description
  end
end