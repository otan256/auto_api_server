class MimeType < RamlModel
  attr_reader :type, :schema, :response, :form_parameters, :action
  def initialize(type, mime_type, parent)
    super(mime_type)
    @type = type
    @response = parent.is_a?(Response) ? parent : nil
    @action = parent.is_a?(Response) ? parent.action : parent
    @schema = Schema.new(process_schema)
    @form_parameters = (mime_type.form_parameters.to_a || []).map { |name, form_parameter| FormParameter.new(name, form_parameter.to_a.first)}
    ParameterValidator.validate_examples(form_parameters, action.resource.url)
  end

  def schema?
    !!schema
  end

  def example?
    raml.example && raml.example != ''
  end

  def example
    JSON.parse(raml.example)
  end

  private
  def process_schema
    return nil if raml.schema.nil? || raml.schema == ''

    schema = safe_json_parse(raml.schema)
    return schema if schema

    schemas = action.resource.document.schemas
    return nil unless schemas && schemas.length > 0

    result = safe_json_parse(schemas[0][raml.schema])
    raise "Couldn't parse schema for #{action.resource.url}: #{raml.schema}" unless result
    result
  end

  def safe_json_parse(json)
    return nil unless json && json != ''
    JSON.parse(json)
  rescue JSON::ParserError
    nil
  end

end