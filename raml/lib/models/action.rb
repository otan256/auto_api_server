require File.dirname(__FILE__) + '/base'
class Action < RamlModel
  attr_reader :verb, :headers, :query_parameters, :uri_parameters, :responses, :full_url, :resource, :mime_types
  def initialize(verb, action, full_url, resource)
    super(action)
    @verb = verb.to_s
    @full_url = full_url
    @resource = resource
    @headers = action.headers.map { |name, header| Header.new(name, header)}
    @query_parameters = action.query_parameters.map { |name, query_parameter| QueryParameter.new(name, query_parameter)}
    @uri_parameters = resource.uri_parameters.map { |name, uri_parameter| UriParameter.new(name, uri_parameter)}
    @responses = action.responses.map { |status_code, response| Response.new(status_code, response, self)}
    @mime_types = action.body.map { |type, mime_type| MimeType.new(type, mime_type, self)}
    ParameterValidator.validate_examples(query_parameters + uri_parameters + url_encoded_params, resource.url)
  end

  def description
    raml.description
  end

  def headers?
    headers.length != 0
  end

  def query_parameters?
    query_parameters.length != 0
  end

  def all_parameters
    query_parameters + uri_parameters + url_encoded_params
  end

  def responses?
    responses.length != 0
  end

  def response_status_codes
    responses.map(&:status_code).map(&:to_s).join(', ')
  end

  def required_parameters
    all_parameters.select { |f| f.required }
  end

  def optional_parameters
    all_parameters.select { |f| !f.required }
  end

  def error_responses
    responses.select { |r| r.error_response? }
  end

  private
  def url_encoded_params
    form_mime_type = 'application/x-www-form-urlencoded'
    params = mime_types.find { |m| m.type == form_mime_type}
    params.nil? ? [] : params.form_parameters
  end
end