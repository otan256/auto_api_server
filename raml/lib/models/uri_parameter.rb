class UriParameter < RamlModel
  attr_reader :name, :description
  def initialize(name, uri_parameter)
    super(uri_parameter)
    @name = name
    @description = uri_parameter.description
  end
end