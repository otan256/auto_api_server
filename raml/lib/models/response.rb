class Response < RamlModel
  attr_reader :status_code, :mime_types, :action
  def initialize(status_code, response, action)
    super(response)
    @status_code = status_code
    @action = action
    @mime_types = response.body.map { |type, mime_type| MimeType.new(type, mime_type, self)}
  end

  def default
    media_type = action.resource.document.media_type
    mime_types.find { |m| m.type == media_type}
  end

  def description_html
    #Maruku.new(description, :on_error => :raise).to_html
    Maruku.new(description).to_html
  end

  def error_response?
    status_code != "200"
  end
end