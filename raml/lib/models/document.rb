class Document < RamlModel
  attr_reader :resources, :options, :flat_resources
  def initialize(document, options)
    super(document)
    @options = options
    @resources = document.resources.map { |url, resource| Resource.new(url, resource, self)}
    @flat_resources = []
    populate_resources(resources)
  end

  def full_url
    base_uri.gsub('{version}', version)
  end

  private
  def populate_resources(raml_resources)
    raml_resources.each do |r|
      if options.filter.length != 0
        next unless options.filter.any? { |f| r.full_url.include?(f) }
      end

      flat_resources << r
      populate_resources(r.resources)
    end
  end
end