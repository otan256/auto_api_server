class ParameterValidator
  def self.validate_examples(parameters, url)
    parameters_missing_examples = parameters.select { |p| p.example.nil? || p.example == ''}
    return unless parameters_missing_examples.any?

    lines = ["The following parameters are missing examples for #{url}"]
    lines += parameters_missing_examples.map do |p|
      puts "\t#{p.name}\n"
    end
    raise lines.join
  end
end