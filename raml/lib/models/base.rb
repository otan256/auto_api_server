class RamlModel
  attr_reader :raml
  def initialize(raml)
    @raml = raml
  end

  def method_missing(m, *args, &block)
    raml.send(m, *args, &block)
  end
end
