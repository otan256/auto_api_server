class Schema
  attr_reader :json, :description
  def initialize(json)
    @json = json || {}
    @description = @json['description']
  end

  def properties
    @properties ||= gather_properties(json).flatten
  end

  def properties?
    !properties.empty?
  end

  private
  def gather_properties(json_hash)
    properties = json_hash['properties']
    if properties
      results = properties.select { |key, hash| hash.is_a?(Hash) && key != 'properties'}.map { |key, hash| { 'name' => key, 'description' => hash['description'] } }
      results += gather_properties(properties) if properties['properties']
      results
    else
      []
    end
  end
end