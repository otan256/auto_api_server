class QueryParameter < RamlModel
  attr_reader :name
  def initialize(name, query_parameter)
    super(query_parameter)
    @name = name
  end

  def type
    raml.type.to_s.downcase
  end

  def description?
    description && description != ''
  end

  def example?
    example && example != ''
  end

  def description
    raml.description
  end

  def example
    raml.example
  end
end