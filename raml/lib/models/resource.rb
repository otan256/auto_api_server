class Resource < RamlModel
  attr_reader :url, :resources, :actions, :document
  def initialize(url, resource, document)
    super(resource)
    @url = url
    @document = document
    @resources = resource.resources.map { |resource_url, resource| Resource.new(url + resource_url, resource, document)}
    @actions = resource.actions.map { |verb, action| Action.new(verb, action, full_url, self)}

    raise "Missing displayName: #{url}" unless raml.display_name
  end

  def full_url
    "#{document.full_url}#{url}"
  end
end