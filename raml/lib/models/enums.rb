module ActionType
  GET     = org.raml.model.ActionType::GET
  POST    = org.raml.model.ActionType::POST
  PUT     = org.raml.model.ActionType::PUT
  DELETE  = org.raml.model.ActionType::DELETE
  HEAD    = org.raml.model.ActionType::HEAD
  PATCH   = org.raml.model.ActionType::PATCH
  OPTIONS = org.raml.model.ActionType::OPTIONS
  TRACE   = org.raml.model.ActionType::TRACE
end