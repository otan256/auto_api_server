class Header < RamlModel
  attr_reader :name
  def initialize(name, header)
    super(header)
    @name = name
  end
end