require './json_to_md'

class MarkdownGenerator

  attr_reader :files_to_convert, :converter

  def initialize(params={})
    @files_to_convert = params.fetch(:files_to_convert, Dir["./raml_spec/**/*error_codes.json"])
    @converter = JsonToMarkdownConverter.new('')
  end

  def run!
    files_to_convert.each do |filename|
      puts "Trying to convert #{filename}"
      converter.json_data = load_file filename

      markdown_data = converter.convert

      save_markdown_file(filename, markdown_data)
    end
  end

  private

  def save_markdown_file(filename, content)
    md_filename = markdown_filename(filename)
    puts "Saving Markdown to #{md_filename}"
    File.write md_filename, content
  end

  def markdown_filename(filename)
    filename.gsub('.json', '.md')
  end

  def load_file(filename)
    File.read filename
  end

end
