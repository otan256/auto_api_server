require 'json'

class JsonToMarkdownConverter

  attr_accessor :json_data

  def initialize(json_data)
    @json_data = json_data
  end

  def convert
    hash = JSON.parse(json_data)

    error_code = hash["error_code"]
    error_messages = hash["error_messages"]

    result = []

    result << set_error_code(error_code)
    result << set_error_messages(error_messages)

    result.join("\n")
  end

  private

  def set_error_code(error_code)
    "Error code: #{error_code}\n==================================="
  end

  def set_error_messages(error_messages)
    result = []
    result << "\n## Error messages ##"

    error_messages.each do |attribute, hashes|
      result << "\n* #{attribute}\n"

      result << "| Code  | Message | Parameters |\n| ----- | ------- | ---------- |"

      hashes.each do |hash|
        result << "| #{hash["code"]} | #{hash["message"]} | #{error_message_params(hash["params"])} |"
      end

    end

    result.join("\n")
  end

  def error_message_params(params)
    return '' if params.nil? || params.empty?

    params.map { |key, value| "#{key}: #{value}" }.join(', ')
  end
end
