Error code: auth_invalid_user_login_details
===================================

## Error messages ##

* email

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| email_length_is_invalid | email should match regexp | regexp: /A.+@.+..+z/ |

* password

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| password_is_too_short | password cannot be shorter than 2 character(s) | minlength: 2 |
| password_is_too_long | password cannot be longer than 255 character(s) | maxlength: 255 |
| password_is_required | password is required |  |