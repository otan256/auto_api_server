Error code: find_cars_failed
===================================

## Error messages ##

* name

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| name_is_too_short | name can not be shorter than 1 character(s) | minlength: 1 |
| name_is_too_long | name can not be longer than 255 character(s) | maxlength: 255 |
| name_is_required | name is required |  |

* model

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| model_is_too_short | model can not be shorter than 1 character(s) | minlength: 1 |
| model_is_too_long | model can not be longer than 255 character(s) | maxlength: 255 |
| model_is_required | model is required |  |

* vin_code

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| vin_code_is_required | vin_code is required |  |

* manufacture_year

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| manufacture_year_is_required | manufacture_year is required |  |

* volume

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| volume_is_too_long | volume can not be longer than 150 character(s) | maxlength: 255 |

* mileage

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| mileage_is_required | mileage is required |  |