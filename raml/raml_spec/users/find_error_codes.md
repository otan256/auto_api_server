Error code: find_users_failed
===================================

## Error messages ##

* name

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| first_name_is_too_short | first_name can not be shorter than 1 character(s) | minlength: 1 |
| first_name_is_too_long | first_name can not be longer than 255 character(s) | maxlength: 255 |
| first_name_is_required | first_name is required |  |

* model

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| car_model_is_too_short | car_model can not be shorter than 1 character(s) | minlength: 1 |
| car_model_is_too_long | car_model can not be longer than 255 character(s) | maxlength: 255 |
| car_model_is_required | car_model is required |  |

* vin_code

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| vin_code_is_required | vin_code is required |  |

* manufacture_year

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| birthday_is_required | birthday is required |  |

* volume

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| phone_is_too_long | phone can not be longer than 150 character(s) | maxlength: 255 |

* mileage

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| address_is_required | address is required |  |