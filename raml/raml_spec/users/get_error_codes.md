Error code: get_user_failed
===================================

## Error messages ##

* id

| Code  | Message | Parameters |
| ----- | ------- | ---------- |
| id_is_too_short | id can not be shorter than 1 character(s) | minlength: 1 |
| id_is_too_long | id can not be longer than 255 character(s) | maxlength: 255 |
| id_is_required | id is required |  |