puts "Change detected"

require 'optparse'
require File.dirname(__FILE__) + '/lib/options'
options = Options.new
if options.display_help
  puts options.parser.help
  exit(2)
end

#require java code
Dir['java_lib/*.jar'].sort.each { |j| require j}

require 'bundler'
require 'fileutils'
Bundler.require(:default)

Dir['lib/**/**.rb'].sort.each { |f| require f}

#generate markdown out of json
require './markdown_generator.rb'

params = {:files_to_convert => Dir[File.dirname(__FILE__) + '/raml_spec/**/*error_codes.json']}
MarkdownGenerator.new(params).run!

#validate
raml_utils = RamlUtils.new(options)
validations = raml_utils.validate
if !validations.empty?
  puts "Validation failures:\n"
  validations.each do |v|
    puts "#{v[:verb]} #{v[:url]} (#{v[:status_code]})" if v[:url]
    puts "\t#{v[:message]}"
  end
  exit(1)
end

#parse
raml = raml_utils.parse

if options.dump
  puts raml_utils.dump
else
  #generate
  generator = case options.generator
                when :console then ConsoleGenerator.new(raml)
                when :wikihtml then WikiHtmlGenerator.new(raml)
                when :errorhtml then ErrorHtmlGenerator.new(raml)
              end

  result = generator.generate
  puts result if options.generator == :console
end
puts "Finished generation" if options.generator != :console

