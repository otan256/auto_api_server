### What is RAML?
[RAML](http://raml.org/) stands for RESTful API Modeling Language and is an open standard that is being shepherded by [Mulesoft](http://www.mulesoft.com/). While not widely adopted yet, the [current version](https://github.com/raml-org/raml-spec) has a lot of useful features that make it easy to describe a REST API. The Auto Catalog has adopted this standard to fully document the second version of their API.

### Setting up your dev environment
#### JRuby
Because RAML is still an emerging standard, there is currently no ruby parser for RAML. However, Mulesoft created [a java implementation](https://github.com/raml-org/raml-java-parser) which we can leverage together with [JRuby](http://jruby.org/). In order to run multiple versions of Ruby, it makes sense to install and use Ruby Version Manager ([RVM](https://rvm.io/)) to manage this.

#### Bundler
The RAML parser has a few gem dependencies which can be handled by [Bundler](http://bundler.io/). By navigating into the raml directory where this repo is cloned, this should create a gemset called 'raml' which will be specific to JRuby. Once inside the directory, execute the following:

* rvm use jruby-1.7.8@raml
* bundle

All necessary dependencies should be installed

#### Guard
When updating the spec or the code for the generators, it can be useful to have something that automatically reruns validation and generation. This is performed with [Guard](http://guardgem.org/) and setup through a simple [Guardfile](https://github.com/guard/guard/wiki/Guardfile-examples) (provided in the repo). After the running the above steps, you should be able to run guard:

* guard

#### Generating HTML templates from raml
This will start monitoring the current directory and output some basic html to the 'output' directory. The command it is running is the following:

* ruby parse_raml.rb --raml raml_spec/api_server_v2.yaml -g wikihtml -o output

### Dealing with validation errors
#### RAML validation errors
```
  Line: 43 - could not found expected ':'
```

Unfortunately, the raml parser doesn't handle the case where there is a root raml file (e.g. api_server_v1.yaml) and included raml files (e.g. balances/find). While it will correctly report an error, it won't be able to tell you which file it is in. It's recommended that you use guard so you get more granular feed back on changes as you make them.

#### Invalid JSON
```
POST /authenticate/api (200)
  unexpected token at '{"auth_token": "043cc6069f5e32757647641460d1d5ed}
'
```

This is quite descriptive and it will tell you exactly which route the problem is. Since there is only one example per route, if should be easy to see where the issue is.

```
  Line: 7 - invalid JSON schema (AuthToken): Illegal unquoted character ((CTRL-CHAR, code 10)): has to be escaped using backslash to be included in string value
```

This is an example when there is malformed JSON in one of the schema files. There should only be one instance of the schema file (in this case 'AuthToken') so it should be relatively easy to find the error

#### Invalid JSON Schema
```
GET /cars/{id} (200)
  The property '#/' did not contain a required property of 'name' in schema 7020ea40-47d6-5650-8ec2-9987025d757e#
```

This is quite descriptive and it will tell you exactly which route the problem is. Since there is only one example per route, if should be easy to see where the issue is.

#### Useful links
* [JSON Lint](http://jsonlint.com/)
* [JSON Schema Generator](http://www.jsonschema.net/#)
* [JSON Schema Lint](http://jsonschemalint.com/)

### How does this code work?
#### java_lib
The java_lib directory contains the raml-parser (v0.8) and all its dependencies. These are all loaded on startup by raml_parser are used to load and validate the raml spec.

#### Schema validation
It's important to note that while the raml is explicitly validated, the JSON schema validation can only be performed after the RAML files are loaded. This happens in the raml_utils.rb file. It's also important to note that the RAML Java parser does not automatically add the JSON Schemas specified in the root RAML file. This is also done programmatically in the Ruby code so that we avoid duplication of the schemas.

#### Models
These are convenience wrappers around the actual java models. If a non-defined property is called on one of these models it will be forwarded on to the java model. The ruby model is a good place to locate convenience methods that are useful during code generation. It's important to keep these models as clean as possible and not pollute them with generator specific methods.

#### Generators
Generators combine models with templates to create a new output for the spec. This could be a text file, html or even actual program code. It all depends on what the purpose of the generator is. Two examples currently in the repo are 'console' and 'wikihtml'. These have their own directories and generator specific code and templates should all be collected in this directory.







