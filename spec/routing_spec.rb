require 'spec_helper'

describe 'Routing errors' do
  include Rack::Test::Methods

  def app
    @app ||= ApiServer::Status.new
  end

  let (:valid_uri) {'/v1/status'}
  let (:invalid_uri) {'/v1/status/unknown'}
  let (:error_response) do
    {
        "error_code" => "resource_not_found",
        "error_messages" => {
            "base" => [{
                           "code" => "resource_not_found",
                           "message" => "Resource not found"
                       }]
        }
    }
  end

  it 'raises 404 error when goes to unknown route' do
    get invalid_uri
    expect(last_response.status).to eq 404
    response = JSON.parse(last_response.body)
    expect(response).to eq error_response
  end

  it "doesn't raise 404 when it is on valid route" do
    get valid_uri
    expect(last_response.status).to eq 200
  end
end