require 'spec_helper'

describe ApiServer::Base do
  include Rack::Test::Methods

  def app
    @app ||= ApiServer::Users.new
  end

  let(:expected_fields) do
    %w(id first_name last_name car_model car_number vin_code birthday phone address created_at updated_at)
  end

  describe 'Register user' do
    it "registers user" do
      post "/v1/register", {
          :email => 'otan256@gmail.com',
          :password => 'welcome',
          :first_name => 'Ostap',
          :last_name => 'Ivanyshyn',
          :birthday => '1994-05-04',
      }

      expect(last_response.status).to eq(200)
      actual_fields = last_response_json.keys
      expect(actual_fields.sort).to eq expected_fields.sort

    end
  end

  describe "User get" do
    it "gets user" do
      get "/v1/users/1", {}, {'HTTP_X_AUTH_TOKEN' => '3d2fc99e52427888a8c02c96bbe8e313'}

      expect(last_response.status).to eq(200)
      actual_fields = last_response_json.keys
      expect(actual_fields.sort).to eq expected_fields.sort

    end
  end
end