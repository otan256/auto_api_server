require 'spec_helper'

describe ApiServer::Base do
  include Rack::Test::Methods

  def app
    @app ||= ApiServer::Cars.new
  end

  let(:expected_fields) do
    %w(id vin_code name model manufacture_year volume mileage created_at updated_at)
  end

  describe "Car Create" do
    it "create car" do

      params = {
          :vin_code => 23421341,
          :name => 'Ford',
          :model => 'Mustang',
          :manufacture_year => 2009,
          :volume => '2.5 l',
          :mileage => 234211,
      }
      post "/v1/cars/create", params, {'HTTP_X_AUTH_TOKEN' => '3d2fc99e52427888a8c02c96bbe8e313'}

      expect(last_response.status).to eq(200)
      actual_fields = last_response_json.keys
      expect(actual_fields.sort).to eq expected_fields.sort

    end
  end

  describe "Car get" do
    it "gets car" do

      get "/v1/cars/#{1}", {}, {'HTTP_X_AUTH_TOKEN' => '3d2fc99e52427888a8c02c96bbe8e313'}

      expect(last_response.status).to eq(200)
      actual_fields = last_response_json.keys
      expect(actual_fields.sort).to eq expected_fields.sort

    end
  end
end