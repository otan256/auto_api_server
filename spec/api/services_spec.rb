require 'spec_helper'

describe ApiServer::Base do
  include Rack::Test::Methods

  def app
    @app ||= ApiServer::Services.new
  end

  let(:expected_fields) do
    %w(id kind_of_service standart_hours standart_hours_price labor_input_work car_model created_at updated_at)
  end

  describe "Service Create" do
    it "create service" do

      params = {
          :kind_of_service => 'Equipment',
          :standart_hours => '24',
          :standart_hours_price => '12',
          :labor_input_work => '12',
          :car_model => 'AT123df v2',
      }
      post "/v1/services/create", params, {'HTTP_X_AUTH_TOKEN' => '3d2fc99e52427888a8c02c96bbe8e313'}

      expect(last_response.status).to eq(200)
      actual_fields = last_response_json.keys
      expect(actual_fields.sort).to eq expected_fields.sort

    end
  end

  describe "Service get" do
    it "gets service" do

      get "/v1/services/#{1}", {}, {'HTTP_X_AUTH_TOKEN' => '3d2fc99e52427888a8c02c96bbe8e313'}

      expect(last_response.status).to eq(200)
      actual_fields = last_response_json.keys
      expect(actual_fields.sort).to eq expected_fields.sort

    end
  end
end