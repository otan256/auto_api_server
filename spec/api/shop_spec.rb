require 'spec_helper'

describe ApiServer::Base do
  include Rack::Test::Methods

  def app
    @app ||= ApiServer::Shop.new
  end

  let(:expected_fields) do
    %w(id product_id name arriving_date price mark_up quantity created_at updated_at)
  end

  describe "Shop Create" do
    it "create shop" do

      params = {
          :product_id => '123123',
          :name => 'Ford',
          :arriving_date => '12',
          :price => '122',
          :mark_up => 'test',
          :quantity => '231',
      }
      post "/v1/shops/create", params, {'HTTP_X_AUTH_TOKEN' => '3d2fc99e52427888a8c02c96bbe8e313'}

      expect(last_response.status).to eq(200)
      actual_fields = last_response_json.keys
      expect(actual_fields.sort).to eq expected_fields.sort

    end
  end

  describe "Shop get" do
    it "gets shop" do

      get "/v1/shops/#{1}", {}, {'HTTP_X_AUTH_TOKEN' => '3d2fc99e52427888a8c02c96bbe8e313'}

      expect(last_response.status).to eq(200)
      actual_fields = last_response_json.keys
      expect(actual_fields.sort).to eq expected_fields.sort

    end
  end
end