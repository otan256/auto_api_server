require 'spec_helper'

describe ApiServer::Base do
  include Rack::Test::Methods

  def app
    @app ||= ApiServer::Orders.new
  end

  let(:expected_fields) do
    %w(id customer_id product_id user_id service_id price mark_up order_date quantity created_at updated_at)
  end

  describe "Order Create" do
    it "create order" do

      params = {
          :customer_id => '45453435',
          :product_id => '1234342',
          :user_id => '245342',
          :service_id => '12341',
          :price => '200000',
          :mark_up => '123213213',
          :order_date => Time.now,
          :quantity => 32,
      }
      post "/v1/orders/create", params, {'HTTP_X_AUTH_TOKEN' => '3d2fc99e52427888a8c02c96bbe8e313'}

      expect(last_response.status).to eq(200)
      actual_fields = last_response_json.keys
      expect(actual_fields.sort).to eq expected_fields.sort

    end
  end

  describe "Order get" do
    it "gets order" do

      get "/v1/orders/#{1}", {}, {'HTTP_X_AUTH_TOKEN' => '3d2fc99e52427888a8c02c96bbe8e313'}

      expect(last_response.status).to eq(200)
      actual_fields = last_response_json.keys
      expect(actual_fields.sort).to eq expected_fields.sort

    end
  end
end